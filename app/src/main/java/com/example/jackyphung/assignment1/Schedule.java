//Jacky Phung
//100801047
//COMP3074

package com.example.jackyphung.assignment1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Schedule extends AppCompatActivity implements View.OnClickListener{

    @Override
    public void onClick(View v) {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        findViewById(R.id.btnMonday).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Planner.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btnTuesday).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Planner.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btnWednesday).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Planner.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btnThursday).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Planner.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btnFriday).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Planner.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btnSaturday).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Planner.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btnSunday).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Planner.class);
                startActivity(i);
            }
        });
    }
}
