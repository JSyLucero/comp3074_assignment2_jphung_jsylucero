//Jacky Phung
//100801047
//COMP3074

package com.example.jackyphung.assignment1;

import android.content.Intent;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Speakers extends AppCompatActivity implements View.OnClickListener{

    private ArrayList<String> items;
    private ArrayAdapter<String> itemsAdapter;
    private ListView lvItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speakers);

        final PostDbHelper dbHelper = new PostDbHelper(this);
        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        lvItems = findViewById(R.id.speakerList);
        items = new ArrayList<>();
        for (int i = 1; i <= (int)DatabaseUtils.queryNumEntries(db, PostContract.PostEntry.TABLE_NAME); i++){
            items.add(dbHelper.getPost(db, i).getName());
        }

        itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);
        lvItems.setAdapter(itemsAdapter);

        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(view.getContext(), Detail.class);
                i.putExtra("speaker", dbHelper.getPost(db, position+1));
                startActivity(i);
            }
        });
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
