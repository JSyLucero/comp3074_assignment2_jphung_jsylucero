//Jacky Phung
//100801047
//COMP3074

package com.example.jackyphung.assignment1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        // Get To-Be-Edited View Items
        TextView dashLabel = findViewById(R.id.dashLabel);

        SharedPreferences prefs = getSharedPreferences("user", MODE_PRIVATE);
        String username = prefs.getString("username", null);
        if (username.length() > 0)
            dashLabel.setText(String.format("Welcome, %s", username));

        findViewById(R.id.btnSchedule).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Schedule.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btnSpeakers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Speakers.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btnAttendees).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Attendees.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btnSponsor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Sponsor.class);
                startActivity(i);
            }
        });

        findViewById(R.id.btnSurvey).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),Survey.class);
                startActivity(i);
            }
        });
    }
}
