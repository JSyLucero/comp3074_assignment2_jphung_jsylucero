package com.example.jackyphung.assignment1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Detail extends AppCompatActivity implements View.OnClickListener{
    private ArrayList<String> items;
    private ArrayAdapter<String> itemsAdapter;
    private ListView lvItems;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent i = getIntent();
        Post speaker = (Post)i.getSerializableExtra("speaker");
        lvItems = findViewById(R.id.detailList);
        items = new ArrayList<>();
        items.add(speaker.getName());
        items.add(speaker.getAffiliation());
        items.add(speaker.getEmail());
        items.add(speaker.getBio());

        itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);
        lvItems.setAdapter(itemsAdapter);
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
