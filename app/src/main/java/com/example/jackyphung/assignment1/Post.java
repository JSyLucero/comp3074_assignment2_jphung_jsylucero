package com.example.jackyphung.assignment1;

import java.io.Serializable;

public class Post implements Serializable {
    private long id;
    private String name;
    private String affiliation;
    private String email;
    private String bio;

    public Post(long id, String name, String affiliation, String email, String bio) {
        this.id = id;
        this.name = name;
        this.affiliation = affiliation;
        this.email = email;
        this.bio = bio;
    }

    public Post(String name, String affiliation, String email, String bio) {
        this.name = name;
        this.affiliation = affiliation;
        this.email = email;
        this.bio = bio;
        this.id = -1;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
}
