//Jacky Phung
//100801047
//COMP3074

package com.example.jackyphung.assignment1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Attendees extends AppCompatActivity implements View.OnClickListener{

    private ArrayList<String> items;
    private ArrayAdapter<String> itemsAdapter;
    private ListView lvItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendees);

        lvItems = findViewById(R.id.attendeeList);
        items = new ArrayList<>();
        items.add("Billy");
        items.add("Ramona");

        itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        lvItems.setAdapter(itemsAdapter);
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
