package com.example.jackyphung.assignment1;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

public class Survey extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        SharedPreferences prefs = getSharedPreferences("user", MODE_PRIVATE);
        final String username = prefs.getString("username", null);

        // Get Activity View Items
        final TextView survey_q1 = findViewById(R.id.txtSurvey_q1);
        final TextView survey_q2 = findViewById(R.id.txtSurvey_q2);
        final TextView survey_q3 = findViewById(R.id.txtSurvey_q3);
        final EditText survey_a1 = findViewById(R.id.txtSurvey_a1);
        final EditText survey_a2 = findViewById(R.id.txtSurvey_a2);
        final EditText survey_a3 = findViewById(R.id.txtSurvey_a3);

        final Context context = Survey.this;
        findViewById(R.id.btnSurveySubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject json = new JSONObject();
                if (!survey_a1.getText().toString().isEmpty()
                        && !survey_a2.getText().toString().isEmpty()
                        && !survey_a3.getText().toString().isEmpty()) {
                    int permission = PermissionChecker.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permission == PermissionChecker.PERMISSION_GRANTED)
                        WriteFile(json);
                    else {
                        ActivityCompat.requestPermissions((Activity)context,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                1);
                        Toast.makeText(getApplicationContext(), "Permission Granted: Please submit again.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Survey must be filled out fully.", Toast.LENGTH_LONG).show();
                }
            }

            private void WriteFile(JSONObject json) {
                try {
                    String appTitle = getResources().getString(R.string.app_name);
                    if (username.length() > 0)
                        json.put("surveyed_user", username);
                    else
                        json.put("surveyed_user", "anonymous");
                    json.put("question_1", survey_q1.getText().toString());
                    json.put("answer_1", survey_a1.getText().toString());
                    json.put("question_2", survey_q2.getText().toString());
                    json.put("answer_2", survey_a2.getText().toString());
                    json.put("question_3", survey_q3.getText().toString());
                    json.put("answer_3", survey_a3.getText().toString());

                    Writer output = null;
                    File file = null;
                    String fileName = "COMP3074_100998164_100801047_" + appTitle + "_survey.txt";
                    if (Build.VERSION.SDK_INT >= 19) {
                        file = new File(Environment.getExternalStorageDirectory(), Environment.DIRECTORY_DOCUMENTS);
                    } else {
                        file = new File(Environment.getExternalStorageDirectory(), "Documents");
                    }

                    if (!file.exists()) {
                        if (file.mkdirs())
                            Log.v("value", "Documents folder was created.");
                    }

                    file = new File(file.getPath(), fileName);
                    output = new BufferedWriter(new FileWriter(file));
                    output.write(json.toString());
                    output.close();

                    Toast.makeText(getApplicationContext(), "Survey Completed and Saved in Documents", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
