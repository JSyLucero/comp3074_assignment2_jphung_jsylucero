package com.example.jackyphung.assignment1;

import android.provider.BaseColumns;

public final class PostContract {
    private PostContract(){}

    public static class PostEntry implements BaseColumns{
        public static final String TABLE_NAME = "SPEAKERS";
        public static final String COL_NAME_NAME = "NAME";
        public static final String COL_NAME_AFFILIATION = "AFFILIATION";
        public static final String COL_NAME_EMAIL = "EMAIL";
        public static final String COL_NAME_BIO = "BIO";
        public static final String COL_TYPE_NAME = "TEXT";
        public static final String COL_TYPE_AFFILIATION = "TEXT";
        public static final String COL_TYPE_EMAIL = "TEXT";
        public static final String COL_TYPE_BIO = "TEXT";
    }

    public static final String SQL_CREATE_POSTS =
            "CREATE TABLE "+PostEntry.TABLE_NAME+" ("+
                    PostEntry._ID+" INTEGER PRIMARY KEY, "+
                    PostEntry.COL_NAME_NAME+" "+PostEntry.COL_TYPE_NAME+", "+
                    PostEntry.COL_NAME_AFFILIATION+" "+PostEntry.COL_TYPE_AFFILIATION+", "+
                    PostEntry.COL_NAME_EMAIL+" "+PostEntry.COL_TYPE_EMAIL+", "+
                    PostEntry.COL_NAME_BIO+" "+PostEntry.COL_TYPE_BIO+")";

    public static final String SQL_DROP_POSTS =
            "DROP TABLE IF EXISTS "+PostEntry.TABLE_NAME;
}
