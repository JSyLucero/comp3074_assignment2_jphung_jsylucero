package com.example.jackyphung.assignment1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class PostDbHelper extends SQLiteOpenHelper {
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "speakers.db";

    public PostDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PostContract.SQL_CREATE_POSTS);
        addPost(db , new Post("Elon Musk", "SpaceX", "elonmusk@tesla.com", "Founder of SpaceX and PayPal. CEO of SpaceX, Tesla, Neuralink"));
        addPost(db , new Post("Jordan B Peterson", "University of Toronto", "jordanbpeterson@uoft.com", "I'm a professor at the University of Toronto and I host controversial discussions"));
        addPost(db , new Post("Joe Rogan", "UFC", "joerogan@jre.com","I run a podcast called Joe Rogan Experince and a host and commentator for the UFC"));
        Log.d("DB-TEST", "Database created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(PostContract.SQL_DROP_POSTS);
        onCreate(db);
        Log.d("DB-TEST", "Database upgraded");
    }

    public long addPost(SQLiteDatabase db, Post post){
        ContentValues values = new ContentValues();
        values.put(PostContract.PostEntry.COL_NAME_NAME, post.getName());
        values.put(PostContract.PostEntry.COL_NAME_AFFILIATION, post.getAffiliation());
        values.put(PostContract.PostEntry.COL_NAME_EMAIL, post.getEmail());
        values.put(PostContract.PostEntry.COL_NAME_BIO, post.getBio());

        return db.insert(PostContract.PostEntry.TABLE_NAME, null, values);
    }

    public Post getPost(SQLiteDatabase db, long postId){
        String[] projection = {
                PostContract.PostEntry._ID,
                PostContract.PostEntry.COL_NAME_NAME,
                PostContract.PostEntry.COL_NAME_AFFILIATION,
                PostContract.PostEntry.COL_NAME_EMAIL,
                PostContract.PostEntry.COL_NAME_BIO
        };

        String selection = PostContract.PostEntry._ID+"= ? ";
        String[] selectionArgs = {Long.toString(postId)};
        Cursor cursor = db.query(
            PostContract.PostEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,
            null,
            null
        );

        if(cursor.moveToNext()){
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(PostContract.PostEntry._ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(PostContract.PostEntry.COL_NAME_NAME));
            String affiliation = cursor.getString(cursor.getColumnIndexOrThrow(PostContract.PostEntry.COL_NAME_AFFILIATION));
            String email = cursor.getString(cursor.getColumnIndexOrThrow(PostContract.PostEntry.COL_NAME_EMAIL));
            String bio = cursor.getString(cursor.getColumnIndexOrThrow(PostContract.PostEntry.COL_NAME_BIO));

            Post post = new Post(id, name, affiliation, email, bio);
            return post;
        }

        return null;
    }
}
