//Jacky Phung
//100801047
//COMP3074

package com.example.jackyphung.assignment1;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Planner extends AppCompatActivity implements View.OnClickListener{

    private ArrayList<String> items;
    private ArrayAdapter<String> itemsAdapter;
    private ListView lvItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planner);

        TextView plannerLabel = findViewById(R.id.plannerLabel);

        SharedPreferences prefs = getSharedPreferences("user", MODE_PRIVATE);
        String username = prefs.getString("username", null);
        if (username.length() > 0)
            plannerLabel.setText(String.format("%s's Planner", username));

        lvItems = findViewById(R.id.planList);
        items = new ArrayList<>();
        items.add("Call Mike");
        items.add("Setup meeting with team");

        itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        lvItems.setAdapter(itemsAdapter);

        Button btn = findViewById(R.id.btnAdd);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et = findViewById(R.id.addText);
                String text = et.getText().toString();
                if(!text.isEmpty()){
                    itemsAdapter.add(text);
                    et.setText("");
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
